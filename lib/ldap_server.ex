defmodule LdapServer do
  use Application

  require Logger
  require LdapServer.Request
  require LdapServer.LDAPv3

  alias LdapServer.Request, as: Request
  alias LdapServer.LDAPv3,  as: LDAP

  # See http://elixir-lang.org/docs/stable/elixir/Application.html
  # for more information on OTP Applications
  def start(_type, _args) do
    import Supervisor.Spec, warn: false

    children = [
      # Define workers and child supervisors to be supervised
      # worker(LdapServer.Worker, [arg1, arg2, arg3]),
      supervisor(Task.Supervisor, [[name: LdapServer.TaskSupervisor]]),
      supervisor(Admin.Repo, []),
      worker(Task, [LdapServer, :listen, [4040]])
    ]

    # See http://elixir-lang.org/docs/stable/elixir/Supervisor.html
    # for other strategies and supported options
    opts = [strategy: :one_for_one, name: LdapServer.Supervisor]
    Supervisor.start_link(children, opts)
  end

  # Start listening on given port and call accept/1 to start accepting incoming
  # connections.
  def listen(port) do
    listen_options = [
      :binary,
      packet: :asn1,
      active: false,
      reuseaddr: true
    ]

    {:ok, socket} = :gen_tcp.listen(port, listen_options)

    Logger.info("Accepting connections on port #{port}.")

    accept(socket)
  end

  # Accept a connection on given socket, start a process to serve this connection
  # and give the new process control over the client connection.
  defp accept(socket) do
    {:ok, client} = :gen_tcp.accept(socket)
    {:ok, pid}    = start_task(client)
    :ok           = :gen_tcp.controlling_process(client, pid)
  end


  defp start_task(client) do
    supervisor = LdapServer.TaskSupervisor
    f          = fn -> serve(client) end

    Task.Supervisor.start_child(supervisor, f)
  end

  # Serve one request:
  # 1. read all available data from the socket
  # 2. decode the data as :LDAPMessage with :ELDAPv3.decode/1
  # 3. provide a proper response for the request
  # 4. encode the response
  # 5. write encoded response back to the socket
  # 6. recurse to wait for the next connection
  #
  # inbetween: log relevant data for debugging
  defp serve(socket) do
    # 1.
    data = case read(socket) do
      {:ok, data}     -> data
      {:error, error} ->
         Logger.info("Connection closed. (#{error})")
         exit(:normal)
    end
    # 2.
    {:ok, request}   = LDAP.decode(data)
    # 3.
    {:ok, response} = Request.handle(request)

    log(:inspect, response)
    # 4.
    message = LDAP.encode(response)

    log(:incoming, request)
    log(:outgoing, response)
    log(:inspect, message)
    # 5.
    write(socket, message)
    # 6.
    serve(socket)
  end

  defp read(socket) do
    :gen_tcp.recv(socket, 0)
  end

  defp write(socket, :unbind) do
    Logger.info("Received an unbind request. Closing connection.")
    close(socket)
    exit(:normal)
  end
  # NOTE: Search requests reply with an array of envelopes
  #       ([search_result_entry, ..., search_result_done]).
  #       Other requests might need the same procedure.
  defp write(socket, [data]) do
    write(socket, data)
  end
  defp write(socket, [data|tail]) do
    # There are no results, so there is also nothing to aggregate into a list.
    write(socket, data)
    write(socket, tail)
  end
  defp write(socket, {:ok, data}) do
    Logger.debug("write {:ok, data}: #{inspect(data)}")
    :gen_tcp.send(socket, data)
  end
  defp write(_socket, {:error, :closed}) do
    Logger.info("Connection was closed. Exiting.")

    exit(:shutdown)
  end

  defp close(socket) do
    :gen_tcp.close(socket)
  end

  defp log(:inspect, data) do
    string = inspect(data)
    Logger.debug("inspect: #{string}")
  end
  defp log(:incoming, data) do
    string = inspect(data, limit: 4096, pretty: true)

    Logger.info("Incoming: #{string}")
    {:ok, data}
  end
  defp log(:outgoing, data) do
    string = inspect(data, limit: 4096, pretty: true)

    Logger.info("Outgoing: #{string}")
    {:ok, data}
  end
  defp log(:error, data) do
    string = inspect(data)
    Logger.error(string)
  end
end
