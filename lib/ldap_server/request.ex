defmodule LdapServer.Request do
  import Record

  require LdapServer.LDAPv3
  alias LdapServer.LDAPv3, as: LDAP

  require Logger
  require Admin.Repo
  require Admin.Entry
  require Admin.Attribute

  alias Admin.Repo
  alias Admin.Entry
  alias Admin.Attribute

  # Search requests need responses consisting of more than one envelope. So the
  # handle function responsible for creating a response to search requests
  # constructs a list of envelopes on its own. In this case it is not needed to
  # wrap the response in an envelope again.
  defp wrap({:ok, response}, _id) do
    {:ok, response}
  end

  defp wrap(response, id) do
    {:ok, LDAP.envelope(message: response, id: id)}
  end

  # Unbind requests are special as they require not to send a response.
  def handle({:LDAPMessage, _, {:unbindRequest, :NULL}, :asn1_NOVALUE}) do
    {:ok, :unbind}
  end

  def handle(data) when is_record(data, :LDAPMessage) do
    id      = LDAP.envelope(data, :id)
    message = LDAP.envelope(data, :message)

    handle(message, id) |> wrap(id)
  end

  def handle({:bindRequest, data}, _id) when is_record(data, :BindRequest) do
    name = LDAP.bind_request(data, :name)

    {:bindResponse,
     LDAP.bind_response(result:     :success,
                        matched_dn: name,
                        message:    "Authenticated!")}
  end

  def handle({:searchRequest, data}, id) when is_record(data, :SearchRequest) do
    import Ecto.Query, only: [from: 2]

    [
      baseObject:   base,
      scope:        scope,
      derefAliases: _defer_aliases,
      sizeLimit:    _size_limit,
      timeLimit:    _time_limit,
      typesOnly:    _types_only,
      filter:       _filter,
      attributes:   _attributes
    ] = LDAP.search_request(data)

    # TODO:
    # - return an array of result entries (<= size_limit) each in an envelope
    #   and a search_result_end at the end

    query = case scope do
      :wholeSubtree -> from entry in Entry, where: like(entry.distinguished_name, ^"%#{base}")
      _             -> from entry in Entry, where: entry.distinguished_name == ^"#{base}"
    end

    entries_bare = Repo.all(query)
    entries = Repo.preload(entries_bare, :attributes)

    search_result_entries = entries |> Enum.map(fn(entry) ->
      attributes = entry.attributes
        |> Enum.reduce(%{}, fn(attribute, acc) ->
          Map.update(acc, "#{attribute.name}", [attribute.value], fn(old_value) ->
            List.flatten([old_value, attribute.value])
          end)
        end)
        |> Enum.reduce([], fn({name, values}, acc) ->
          [LDAP.partial_attribute(type: name, vals: values), acc]
          |> List.flatten
        end)

      search_result_entry = LDAP.search_result_entry(
        objectName: entry.distinguished_name,
        attributes: attributes
      )

      search_result_entry
    end)

    Logger.debug(inspect(search_result_entries))

    #attributes = LDAP.partial_attribute(type: "a", vals: ["test"])

    #search_result_entry = LDAP.search_result_entry(
    #  objectName: "uid=a,ou=people,#{base}",
    #  attributes: [attributes]
    #)

    search_result_done = LDAP.search_result_done(
      result:     :success,
      matched_dn: base,
      message:    "foo"
    )

    wrapped_search_result_entries = Enum.map(search_result_entries, &({:searchResEntry, &1}))

    result = [wrapped_search_result_entries, {:searchResDone, search_result_done}]
    |> List.flatten
    |> Enum.map(fn (message) -> LDAP.envelope(message: message, id: id) end)

    Logger.debug(inspect(result))

    {:ok, result}
  end

  # Handle all not implemented requests and return an operationsError to the
  # client.
  def handle(data, _id) do
    type = Tuple.to_list(data) |> List.first

    Logger.error("Error: request type \"#{type}\" not implemented yet")

    {:searchResDone, LDAP.searchResultDone(result: :operationsError, message: "Request type \"#{type}\" not implemented yet.")}
  end
end
