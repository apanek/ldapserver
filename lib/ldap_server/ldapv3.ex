defmodule LdapServer.LDAPv3 do
  import Record

  defrecord :envelope, :LDAPMessage, [
    :id,
    :message,
    {:controls, :asn1_NOVALUE}
  ]

  defrecord :result, :LDAPResult, [
    :result,
    {:matched_dn, ""},
    {:message, ""},
    {:referral, :asn1_NOVALUE}
  ]

  defrecord :bind_request, :BindRequest, [
    :version,
    :name,
    :authentication
  ]

  defrecord :bind_response, :BindResponse, [
    :result,
    :matched_dn,
    :message,
    {:referral,        :asn1_NOVALUE},
    {:serverSaslCreds, :asn1_NOVALUE}
  ]

  defrecord :substring_filter, :SubstringFilter, [
    :type,
    :substrings
  ]

  defrecord :attribute_value_assertion, :AttributeValueAssertion, [
    :attributeDesc,
    :assertionValue
  ]

  defrecord :partial_attribute, :PartialAttribute, [
    :type,
    :vals
  ]

  defrecord :control, :Control, [
    :controlType,
    {:criticality,  :asn1_DEFAULT},
    {:controlValue, :asn1_NOVALUE}
  ]

  defrecord :sasl_credentials, :SaslCredentials, [
    :mechanism,
    {:credentials, :asn1_NOVALUE}
  ]

  defrecord :search_request, :SearchRequest, [
    :baseObject,
    :scope,
    :derefAliases,
    :sizeLimit,
    :timeLimit,
    :typesOnly,
    :filter,
    :attributes
  ]

  defrecord :matching_rule_assertion, :MatchingRuleAssertion, [
    {:matchingRule, :asn1_NOVALUE},
    {:type,         :asn1_NOVALUE},
    :matchValue,
    {:dnAttributes, :asn1_DEFAULT}
  ]

  defrecord :search_result_entry, :SearchResultEntry, [
    :objectName,
    :attributes
  ]

  defrecord :search_result_done, :SearchResultDone, [
    :result,
    {:matched_dn, ""},
    {:message, ""},
    {:referral, :asn1_NOVALUE}
  ]

  defrecord :modify_request, :ModifyRequest, [
    :object,
    :changes
  ]

  defrecord :changes, :ModifyRequest_changes_SEQOF, [
    :operation,
    :modification
  ]

  defrecord :add_request, :AddRequest, [
    :entry,
    :attributes
  ]

  defrecord :modify_dn_request, :ModifyDnRequest, [
    :entry,
    :newrdn,
    :deleteoldrdn,
    {:newSuperior, :asn1_NOVALUE}
  ]

  defrecord :compare_request, :CompareRequest, [
    :entry,
    :ava
  ]

  defrecord :extended_request, :ExtendedRequest, [
    :requestName,
    {:requestValue, :asn1_NOVALUE}
  ]

  defrecord :extended_response, :ExtendedResponse, [
    :resultCode,
    :matchedDN,
    :diagnosticMessage,
    {:referral,      :asn1_NOVALUE},
    {:responseName,  :asn1_NOVALUE},
    {:responseValue, :asn1_NOVALUE}
  ]

  defrecord :intermediate_response, :IntermediateResponse, [
    {:responseName,  :asn1_NOVALUE},
    {:responseValue, :asn1_NOVALUE}
  ]

  defrecord :password_modify_request, :PasswdModifyRequestValue, [
    {:userIdentity, :asn1_NOVALUE},
    {:oldPasswd,    :asn1_NOVALUE},
    {:newPasswd,    :asn1_NOVALUE}
  ]

  defrecord :password_modify_response, :PasswordModifyResponseValue, [
    {:genPasswd, :asn1_NOVALUE}
  ]

  @doc """
    Encode an envelope
  """

  def encode(:unbind) do
    :unbind
  end

  def encode([data]) do
    encode(data)
  end

  def encode([data|tail]) do
    [encode(data), encode(tail)] |> List.flatten
  end

  def encode(data) do
    :ELDAPv3.encode(:LDAPMessage, data)
  end

  def decode(data) do
    :ELDAPv3.decode(:LDAPMessage, data)
  end
end
