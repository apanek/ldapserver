# Directory.LdapServer

This is an experiment to implement a rudimentary LDAP (v3) compliant directory
service using Elixir.

The goal is to provide an alternative implementation of certain parts of common
implementations in C and Java and extending from there on:

- LDAP server side implementation
- an API to hook into the protocol similar to [ldapjs](http://ldapjs.org)
- an implementation to store directory data in common key-value stores that can
  be accessed through other protocols as well (e.g.:
  [riak](http://basho.com/products/riak-kv/), [couchdb](https://couchdb.apache.org))
- a plain text protocol definition and implementation for accessibility and learning
- providing an alternative HTTP/JSON based protocol/API to ease integration
  with other services in different languages that might not implement ASN.1,
  BER and LDAP out of the box.

The first step is to implement the server side of the protocol to be compliant
with commonly used OpenLDAP client tools (`ldapwhoami`, `ldapsearch`,
`ldapadd`, etc.). With the protocol in place, the next step is implementing a
storage interface to any of the above mentioned key-value stores.
